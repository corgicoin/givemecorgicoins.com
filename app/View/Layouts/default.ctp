<?php echo $this->Html->docType('html5'); ?>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		Give Me Corgi Coins! : <?php echo $title_for_layout; ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<?php
	echo $this->Html->meta('icon');
	echo $this->fetch('meta');

	echo $this->Html->css('bootstrap.min');
	echo $this->Html->css('main.min');
	echo $this->fetch('css');

	echo $this->Html->script('libs/jquery-1.10.2.min');
	echo $this->Html->script('libs/bootstrap.min');
	echo $this->Html->script('respond.min');

	echo $this->Html->script('easing');
	echo $this->Html->script('jquery.flipCounter.1.2.pack');
	echo $this->Html->script('main.min');
	echo $this->fetch('script');
	?>
	<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', '<?php echo Configure::read('Settings.API.google_analytics.key'); ?>', 'givemecorgicoins.com');
		ga('send', 'pageview');
	</script>
</head>

<body>
<div id="main-container">
	<div id="content" class="container">
	
		<div class="stage">
			<div class="logo">
				<a href="/"><img src="/img/givemecorgicoins.png" /></a>
			</div>
			<div class="row">
				<?php echo $this->Session->flash(); ?>
				<?php echo $this->fetch('content'); ?>
			</div>
			<div class="footer">
				<a href="http://corgicoin.com/" target="_new"><img src="/img/funded.png" /></a>
			</div>
		</div>
	</div>
</div>
</body>
</html>