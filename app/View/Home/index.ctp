
<div class="ministage">
	So you want some CorgiCoins do you? Well, you came to the right place. 
	<br/><br/>
	We're going to give you three chances to get up to 1000 CorgiCoins every 24 hours.
	<br/><br/>
	Ready to get started and get your first roll? Just enter your CorgiCoin address below.
</div>

<div class="finalstage" style="display:none;">
	Congratulations, you've been sent <b><span class="your_roll"></span></b> CorgiCoins! Come back tomorrow for another 3 chances at winning up to 1000 CorgiCoins!
	<br/><br/>
	This faucet is funded by the CorgiCoin development team, thanks for supporting our coin - we love you guys!
</div>

<div class="resultstage" style="display:none;">
	Wow, you rolled a <b><span class="your_roll"></span></b>! <span class="follow_up"></span>
</div>

<div class="errorstage" style="display:none;">
</div>

<div class="rollstage" style="display:none;">
	<div id="roller" class="roller"><input type="hidden" name="counter-value" value="100" /></div>
	<div class="rollactions">
		<?php echo $this->Form->button(__('Roll It Again!'), array('id' => 'btnRoll', 'class' => 'btn-lg btn-success btn-roll')); ?>
		<?php echo $this->Form->button(__('Keep This Roll'), array('id' => 'btnKeep', 'class' => 'btn-lg btn-primary btn-keep')); ?>
	</div>
</div>

<div class="actionstage">
	<div class="col">
		<?php echo $this->Form->input('address', array(
			'type' => 'text',
			'id' => 'txtAddress',
			'div' => false,
			'class' => 'form-control',
			'label' => 'Your CorgiCoin Address'
		)); ?>
		<?php echo $this->Form->button(__('ROLL'), array('id' => 'btnRoll0', 'class' => 'btn-lg btn-success btn-roll')); ?>
	</div>
</div>