
<div class="funnel">

	<div class="col-md-12 logo">
		<a href="http://flop.corgicoin.com/"><img src="/img/corgiflop.png" class="floplogo" border="0" alt="Corgi Flop" /></a>
	</div>
	
	<div class="desc">
		Alright! You earned <b><?php echo $amount; ?></b> CorgiCoins with that awesome flopping!
		<br/>
		Just enter your CorgiCoin address below and we will send them to you. 
		<br/><br/>
		What? You don't know what CorgiCoin is? Well, you better go <a href="http://corgicoin.com/">get the client</a> and get mining - you're missing out on the fun!
		<br/><br/>
		And remember to come back in 24 hours and try to beat your high score - we'll give you up to 100 coins a day!
		<br/><br/>
		Want to donate to this faucet? Send CorgiCoins to CQAkkW7rFYeNUXKLQ16Bhj6mSxhxpeNaCj
	</div>
	
	<div class="col-md-12 desc bot">
		<?php echo $this->Form->create('Flop', array(
			'inputDefaults'=> array(
				'div' => 'form-group',
				'label' => array(
					'class' => 'col col-md-3 control-label'
				),
				'wrapInput' => 'col col-md-9',
				'class' => 'form-control'
			),
			'class' => 'form-horizontal'
		)); ?>
		<?php echo $this->Form->input('address', array('label' => false, 'placeHolder' => 'Your CorgiCoin Address')); ?>
		<div class="form-group">
			<?php echo $this->Form->submit(__('Claim Coins!'), array('div' => 'col col-md-12', 'class' => 'btn btn-primary')); ?>
		</div>
	</div>
	
</div>