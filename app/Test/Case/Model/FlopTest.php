<?php
App::uses('Flop', 'Model');

/**
 * Flop Test Case
 *
 */
class FlopTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.flop'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Flop = ClassRegistry::init('Flop');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Flop);

		parent::tearDown();
	}

}
