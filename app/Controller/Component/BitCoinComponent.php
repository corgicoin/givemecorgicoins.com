<?php
App::uses('Component', 'Controller');

class BitCoinComponent extends Component {
	protected $coin;
	protected $passphrase;

	public function initialize(Controller $controller) {
		require_once(APP . 'Vendor' . DS . 'easybitcoin.php');

		## create the connection to the daemon
		$this->coin = new BitCoin(
			Configure::read('Settings.daemon.username'),
			Configure::read('Settings.daemon.password'),
			Configure::read('Settings.daemon.host'),
			Configure::read('Settings.daemon.port')
		);

		$this->passphrase = Configure::read('Settings.account.passphrase');
	}

	public function getInfo() {
		return $this->coin->getinfo();
	}

	public function listAccounts() {
		return $this->coin->listaccounts();
	}

	public function validateAddress($address=null) {
		$isValid = $this->coin->validateaddress($address);

		if($isValid['isvalid']) {
			return true;
		} else {
			return false;
		}
	}

	public function getBalance($account=null) {
		if($account) {
			return $this->coin->getbalance($account);
		} else {
			return 0;
		}
	}

	public function sendFrom($account=null, $address=null, $amount=null) {
		if($account != null && $address != null && $amount != null) {
		
			if ($this->passphrase) {
				$this->coin->walletpassphrase($this->passphrase, 10);
			}

			$this->log(
				"$account; to $address; amount $amount",
				'sendFrom'
			);

			$id = $this->coin->sendfrom($account, $address, (float)$amount, 3);

			$this->log(
				"txid $id",
				'sendFrom'
			);

			if($this->coin->error) {
				$this->log(
					"error {$this->coin->error}",
					'sendFrom'
				);
			}

			return $id;
		} else {
			return null;
		}
	}

	public function getTransaction($id) {
		return $this->coin->gettransaction($id);
	}

	public function listReceivedByAddress() {
		return $this->coin->listreceivedbyaddress();
	}

	public function listTransactions($account=null, $count=10, $from=0) {
		if($account) {
			return $this->coin->listtransactions($account, $count, $from);
		} else {
			return $this->coin->listtransactions();
		}
	}
}