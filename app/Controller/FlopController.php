<?php
App::uses('AppController', 'Controller');

/**
 * Class FlopController
 *
 * @property Transaction $Transaction
 * @property Flop $Flop
 *
 */
class FlopController extends AppController {
	public $uses = array('Transaction', 'Flop');
	public $components = array('BitCoin');
	public $layout = 'flop';

	protected $key = 'kd0j3DD39dk87737jd';

	public function index() {
		$result = array();
		$ip = $_SERVER['REMOTE_ADDR'];

		if(isset($this->request->query['coins'])) {
			$flop = $this->Flop->checkToday($ip);

			if(!$flop || $flop['Flop']['address'] == '') {
				if($flop) {
					## delete current flop record, as it wasn't used
					$this->Flop->delete($flop['Flop']['id']);
				}

				$coins = $this->request->query['coins'];
				$coins = (int) $coins;

				if ($coins > 100) {
					$coins = 1;
				}

				$key = $this->_encrypt($coins . ':' . $ip);

				$result['status'] = 'success';
				$result['data']['key'] = $key;

				$this->Flop->create();
				if($this->Flop->save(array('Flop' => array('ip_address' => $ip, 'key' => $key, 'amount' => (float)$coins)))) {
				}
			} else {
				$result['status'] = 'error';
				$result['data'] = null;
				$result['message'] = 'Already sent';
			}
		} else {
			$result['status'] = 'error';
			$result['data'] = null;
			$result['message'] = 'Invalid Request';
		}

		return parent::jsonResponse($result, '*');
	}

	public function redeem($flopKey=null) {
		if($flopKey) {
			$key = $flopKey;
			$pre = $this->_decrypt($key);
			$amount = (float)substr($pre, 0, strpos($pre, ':'));
			
			if ($amount > 100) { $amount = 1; }
			
			$redeemIP = substr($pre, strpos($pre, ':') + 1);

			if ($this->request->is('post') || $this->request->is('put')) {
				$ip = $_SERVER['REMOTE_ADDR'];
				$address = Sanitize::clean($this->request->data['Flop']['address']);

				if(empty($address)) {
					$this->Flop->invalidate('address', 'Required');
				} else {
					if($ip == $redeemIP) {
						## make sure key and amount are valid for this IP
						
						if ($flop = $this->Flop->find('first', array('conditions' => array('Flop.ip_address' => $ip, 'Flop.key' => $key, 'Flop.amount' => $amount)))) {
							## make sure the flop hasn't already been sent
							if(empty($flop['Flop']['address'])) {
								## make sure the address is valid
								if($this->BitCoin->validateAddress($address)) {
									if($id = $this->BitCoin->sendFrom(Configure::read('Settings.account.flop.name'), $address, $amount)) {
										$this->Session->setFlash(__('CorgiCoins Sent!'), 'flash/success');

										## save the transaction
										$this->Transaction->saveTransaction(array(
											'Transaction' => array(
												'from_account' => Configure::read('Settings.account.flop.name'),
												'ip_address' => $ip,
												'transaction_id' => $id,
												'amount' => $amount
											)
										));

										$this->Flop->id = $flop['Flop']['id'];
										$this->Flop->saveField('address', $address);
										$this->redirect('http://flop.corgicoin.com/sent.html');
										
									} else {
									}
								} else {
									$this->Flop->invalidate('address', 'Invalid Address');
								}
							} else {
								$this->Session->setFlash(__('Coins already sent'), 'flash/error');
							}
						} else {
							$this->Session->setFlash(__('Invalid Flop'), 'flash/error');
						}
					} else {
						$this->Session->setFlash(__('Invalid Flop'), 'flash/error');
					}
				}
			}

			$this->set('title_for_layout', 'Redeem Your CorgiCoins!');
			$this->set('amount', $amount);
		} else {
			$this->redirect('http://flop.corgicoin.com/');
		}
	}

	public function scores() {
		$result = array();

		if($scores = $this->Flop->getHighScoreList(50)) {
			$result['status'] = 'success';
			$result['data'] = $scores;
		} else {
			$result['status'] = 'error';
			$result['data'] = null;
			$result['message'] = __('Invalid Request');
		}

		return parent::jsonResponse($result, '*');
	}

	public function test() {
	}

	private function _encrypt($data) {
		return $this->_base64url_encode(
			mcrypt_encrypt(
				MCRYPT_RIJNDAEL_256,
				md5($this->key),
				$data,
				MCRYPT_MODE_CBC,
				md5(md5($this->key))
			)
		);
	}

	private function _decrypt($key) {
		return rtrim(
			mcrypt_decrypt(
				MCRYPT_RIJNDAEL_256,
				md5($this->key),
				$this->_base64url_decode($key),
				MCRYPT_MODE_CBC,
				md5(md5($this->key))
			),
			"\0"
		);
	}

	private function _base64url_encode($data) {
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
	}

	private function _base64url_decode($data) {
		return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
	}
}
