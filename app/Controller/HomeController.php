<?php
App::uses('AppController', 'Controller');

/**
 * Class HomeController
 *
 * @property Roll $Roll
 * @property Transaction $Transaction
 * @property Donation $Donation
 */
class HomeController extends AppController {
	public $uses = array('Roll', 'Transaction', 'Donation');
	public $components = array('BitCoin');

	public function index() {
		$balance = $this->BitCoin->getBalance(Configure::read('Settings.account.faucet.name'));
		
		if($balance <= Configure::read('Settings.account.faucet.minBalance')) {
			$this->autoRender = false;
			$this->render('faucet_off');
		}
	}

	public function doRoll() {
		if($this->request->is('ajax')) {
			$result = array();

			if($this->BitCoin->getBalance(Configure::read('Settings.account.faucet.name')) >= Configure::read('Settings.account.faucet.minBalance')) {
				if(isset($this->request->query['address'])) {
					$address = $this->request->query['address'];

					## make sure the given address is valid
					if($this->BitCoin->validateAddress($address)) {
						$ip = $_SERVER['REMOTE_ADDR'];

						## make sure user/ip has rolls left today
						if($this->Roll->checkToday($ip, $address)) {
							$result['status'] = 'error';
							$result['data'] = null;
							$result['message'] = "You've already used all your rolls for today";
						} else {
							## get user/ip roll count for today
							$rolls = $this->Roll->find('count', array(
								'conditions' => array(
									'DATE(Roll.created) = CURDATE()',
									'Roll.ip_address' => $ip
								)
							));

							$roll = mt_rand(Configure::read('Settings.account.faucet.minRoll'), Configure::read('Settings.account.faucet.maxRoll'));

							$result['data']['rolls'] = $this->Roll->checkToday($ip, $address);
							$toSave = array(
								'Roll' => array(
									'ip_address' => $ip,
									'address' => $address,
									'value' => $roll,
									'status' => ($rolls == 2) ? 1 : 0
								)
							);

							$keep = true;
							if($rolls == 2) {
								$keep = false;
							}

							$this->Roll->create();
							if($this->Roll->save($toSave)) {
								$result['status'] = 'success';
								$result['data']['message'] = 'saved';
								$result['data']['roll'] = $roll;
								$result['data']['keep'] = $keep;

								if($rolls == 2) {
									## last roll, send it
									$id = $this->BitCoin->sendFrom(
										Configure::read('Settings.account.faucet.name'),
										$address,
										$roll
									);

									$this->Transaction->saveTransaction(array(
										'Transaction' => array(
											'from_account' => Configure::read('Settings.account.faucet.name'),
											'ip_address' => $ip,
											'transaction_id' => $id,
											'amount' => $roll
										)
									));

									$result['data']['trans_id'] = $id;
								}
							}
						}
					} else {
						$result['status'] = 'error';
						$result['data'] = null;
						$result['message'] = 'Invalid Address';
					}
				} else {
					## invalid request; missing address
					$result['status'] = 'error';
					$result['data'] = null;
					$result['message'] = 'Invalid Request';
				}
			} else {
				$result['status'] = 'error';
				$result['data'] = null;
				$result['message'] = 'Faucet Closed';
			}

			return new CakeResponse(array('body' => json_encode($result)));
		}

		return false;
	}

	public function keepRoll() {
		if($this->request->is('ajax')) {
			$result = array();

			$ip = $_SERVER['REMOTE_ADDR'];

			## get the last roll for the given IP
			if($roll = $this->Roll->getLastRollByIP($ip)) {
				if($this->BitCoin->getBalance(Configure::read('Settings.account.faucet.name')) >= Configure::read('Settings.account.faucet.minBalance')) {
					$this->Roll->id = $roll['Roll']['id'];
					$this->Roll->saveField('status', 1);

					$result['status'] = 'success';
					$result['data']['message'] = 'Roll saved, come back tomorrow';

					$id = $this->BitCoin->sendFrom(
						Configure::read('Settings.account.faucet.name'),
						$roll['Roll']['address'],
						$roll['Roll']['value']
					);

					$this->Transaction->saveTransaction(array(
						'Transaction' => array(
							'from_account' => Configure::read('Settings.account.faucet.name'),
							'ip_address' => $ip,
							'transaction_id' => $id,
							'amount' => $roll['Roll']['value']
						)
					));

					$result['data']['trans_id'] = $id;
				} else {
					$result['status'] = 'error';
					$result['data'] = null;
					$result['message'] = 'Faucet Closed';
				}
			} else {
				$result['status'] = 'error';
				$result['data'] = null;
				$result['message'] = 'Invalid Request';
			}

			return new CakeResponse(array('body' => json_encode($result)));
		}

		return false;
	}

	public function checkDonations() {
		## get a list of recent transactions
		$trans = $this->BitCoin->listTransactions(Configure::read('Settings.account.faucet.name'), 50);

		$count = 0;

		foreach($trans as $tran) {
			if($tran['category'] == 'receive') {   ## donation
				## see if we have this donation stored already
				if(!$check = $this->Donation->findByTransactionId($tran['txid'])) {
					## NEW
					$this->Donation->create();
					$this->Donation->save(array(
						'Donation' => array(
							'transaction_id' => $tran['txid'],
							'address' => $tran['address'],
							'amount' => $tran['amount']
						)
					));

					$count++;
				}
			}
		}

		$this->set('count', $count);
	}
}
