<?php
App::uses('AppController', 'Controller');

/**
 * Class StatsController
 *
 * @property Transaction $Transaction
 * @property Flop $Flop
 * @property Roll $Roll
 *
 */
class StatsController extends AppController {
	public $uses = array('Transaction', 'Flop', 'Roll');

	public function index() {
		$this->set('rolls_total', $this->Roll->find('count'));
		$this->set('rolls_today', $this->Roll->find('count', array('conditions' => array('DATE(Roll.created) = CURDATE()'))));

		$this->set('flops_total', $this->Flop->find('count'));
		$this->set('flops_today', $this->Flop->find('count', array('conditions' => array('DATE(Flop.created) = CURDATE()'))));
	}
}
