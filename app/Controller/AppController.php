<?php
App::uses('Controller', 'Controller');
App::uses('Sanitize', 'Utility');
/**
 * CakePHP Component & Model Code Completion
 * @author junichi11
 *
 * ==============================================
 * CakePHP Core Components
 * ==============================================
 * @property AuthComponent $Auth
 * @property AclComponent $Acl
 * @property CookieComponent $Cookie
 * @property EmailComponent $Email
 * @property RequestHandlerComponent $RequestHandler
 * @property SecurityComponent $Security
 * @property SessionComponent $Session
 */

class AppController extends Controller {
	var $helpers = array(
		'Session',
		'Html' => array('className' => 'BoostCake.BoostCakeHtml'),
		'Form' => array('className' => 'BoostCake.BoostCakeForm'),
		'Paginator' => array('className' => 'BoostCake.BoostCakePaginator'),
	);
	var $components = array(
		'Session',
	);

	public function beforeFilter() {
	}

	public function jsonResponse($data, $CORS="") {
		array_merge(array(
				'status' => 'error',
				'data' => null,
				'message' => __('Unknown Error')
			),
			$data
		);

		$response = new CakeResponse(array(
			'body' => json_encode($data),
			'type' => 'json',
		));

		if(!empty($CORS)) {
			$response->header('Access-Control-Allow-Origin', $CORS);
		}

		return $response;
	}
}
