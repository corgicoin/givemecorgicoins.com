var count = 0,
clickable = true;
			    
$(document).ready(function() {

/*
	$('ul').roundabout({
		shape: "waterWheel"
	});
*/

	$("#roller").flipCounter(); 
	$("#roller").flipCounter({imagePath:"img/flipCounter-large.png",digitHeight:120,digitWidth:90, number:999});

	$('.errorstage').click(function() { $(this).fadeOut(); });

	$('#btnRoll0').on('click', function() {
	
		//applyCount(0);
		var btn = $(this);
		var address = $('#txtAddress').val();

		btn.attr('disabled', true);

		if(address == '') {
			$('.errorstage').html('You must enter a CorgiCoin address!').fadeIn();
			btn.attr('disabled', false);
			return false;
		}

		$.getJSON('/doRoll', { address: address }, function(response) {
			if(response.status == 'error') {
				$('.errorstage').html(response.message + '!').fadeIn();
			} else {

				if (!response.data.keep) {

					$('.ministage').hide();
					$('.actionstage').hide();

					$('.your_roll').text(response.data.roll);
					$('.resultstage').hide();
					$('.rollstage').hide();
					$('.finalstage').fadeIn();

				} else {
			
					// stage the responses.. 
					$('.your_roll').text(response.data.roll);
					$('.follow_up').html('You can roll again, or choose to keep this roll - it\'s up to you!<br/><br/>Remember, if you roll lower you lose this roll!');
	
					$('.ministage').hide();
					$('.actionstage').hide();
	
					$('.resultstage').show();
					$('.rollstage').fadeIn("slow", function() {
//						applyCount(response.data.roll);

						$("#roller").flipCounter(
						        "startAnimation", // scroll counter from the current number to the specified number
						        {
						                number: 999,
						                end_number: response.data.roll,
						                easing: jQuery.easing.easeOutCubic,
						                duration: 2500, 
						        }
						);		
					});

				}
			}

			btn.attr('disabled', false);
		});
	});
	
	$('#btnRoll').on('click', function() {
	
		var btn = $(this);
		var address = $('#txtAddress').val();

		btn.attr('disabled', true);

		if(address == '') {
			$('.errorstage').html('You must enter a CorgiCoin address!').fadeIn();
			btn.attr('disabled', false);
			return false;
		}

		$.getJSON('/doRoll', { address: address }, function(response) {
			if(response.status == 'error') {
				$('.errorstage').html(response.message + '!').fadeIn();
			} else {

				if (!response.data.keep) {

					$('.your_roll').text(response.data.roll);
					$('.resultstage').hide();
					$('.rollstage').hide();
					$('.finalstage').fadeIn();

				} else {
					$('.your_roll').text(response.data.roll);
					$('.follow_up').html('You can roll again, or choose to keep this roll - it\'s up to you!<br/><br/>Remember, if you roll lower you lose this roll!');
//					applyCount(response.data.roll);
					

					$("#roller").flipCounter(
					        "startAnimation", // scroll counter from the current number to the specified number
					        {
						                end_number: response.data.roll,
						                easing: jQuery.easing.easeOutCubic,
						                duration: 2500,
					        }
					);					
										
				}
				
			}

			btn.attr('disabled', false);
		});
	});

	$('#btnKeep').on('click', function() {
		var btn = $(this);

		btn.attr('disabled', true);

		$.getJSON('/keepRoll', {}, function(response) {
			if (response.status == 'error') {
				$('.errorstage').html(response.message + '!').fadeIn();
			} else {
				$('.your_roll').text(response.data.roll);
				$('.resultstage').hide();
				$('.rollstage').hide();
				$('.finalstage').fadeIn();				
			}

			btn.addClass('hidden');
		});
	});
			
});


function applyCount(total) {
	var i, part, child, factor, distance,
	    count = new String(total),
	    parts = count.split("").reverse();

	for (i = parts.length - 1; i >= 0; i--) {
		part = parseInt(parts[i], 10);

		child = $('ul#digit-' + i).data('roundabout').childInFocus;
		factor = (part < child) ? (10 + part) - child : part - child;
		distance = factor * 36;
		
		if (i) {
			$('ul#digit-' + i).roundabout('animateToDelta', -distance);
		} else {
			$('ul#digit-' + i).roundabout('animateToDelta', -distance, function() {
/*
				$('.interact a').fadeTo(100, 1);
				clickable = true;
*/
			});
		}
		
	}
}
			
