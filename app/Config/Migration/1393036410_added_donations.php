<?php
class AddedDonations extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 * @access public
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 * @access public
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'donations' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
					'transaction_id' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 64, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
					'address' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 34, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
					'amount' => array('type' => 'float', 'null' => false, 'default' => NULL, 'length' => '16,8'),
					'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
					'modified' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
					'indexes' => array(
						'PRIMARY' => array('column' => 'id', 'unique' => 1),
						'address' => array('column' => 'address', 'unique' => 0),
						'transaction_id' => array('column' => 'transaction_id', 'unique' => 0),
					),
					'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB'),
				),
			),
			'alter_field' => array(
				'flops' => array(
					'ip_address' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 15, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
					'address' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 34, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
					'key' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 100, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
					'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL, 'key' => 'index'),
				),
				'rolls' => array(
					'ip_address' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 15, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
					'address' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 34, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
					'status' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index', 'comment' => '0=not used, 1=used'),
					'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL, 'key' => 'index'),
				),
			),
			'create_field' => array(
				'flops' => array(
					'indexes' => array(
						'ip_address' => array('column' => 'ip_address', 'unique' => 0),
						'address' => array('column' => 'address', 'unique' => 0),
						'key' => array('column' => 'key', 'unique' => 0),
						'created' => array('column' => 'created', 'unique' => 0),
					),
				),
				'rolls' => array(
					'indexes' => array(
						'ip_address' => array('column' => 'ip_address', 'unique' => 0),
						'address' => array('column' => 'address', 'unique' => 0),
						'status' => array('column' => 'status', 'unique' => 0),
						'created' => array('column' => 'created', 'unique' => 0),
					),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'donations'
			),
			'alter_field' => array(
				'flops' => array(
					'ip_address' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
					'address' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 34, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
					'key' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
					'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
				),
				'rolls' => array(
					'ip_address' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
					'address' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 34, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
					'status' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'comment' => '0=not used, 1=used'),
					'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
				),
			),
			'drop_field' => array(
				'flops' => array('', 'indexes' => array('ip_address', 'address', 'key', 'created')),
				'rolls' => array('', 'indexes' => array('ip_address', 'address', 'status', 'created')),
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function after($direction) {
		return true;
	}
}
