<?php
App::uses('AppModel', 'Model');
/**
 * Flop Model
 *
 */
class Flop extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'ip_address' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),

			),
		),
		'address' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
		'key' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
		'amount' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
	);

	public function checkToday($ip) {
		$flop = $this->find('first', array(
			'conditions' => array(
				'DATE(Flop.created) = CURDATE()',
				'Flop.ip_address' => $ip
			)
		));

		return $flop;
	}

	public function getHighScoreList($limit=50) {
		return $this->find('all', array(
			'conditions' => array(
				'Flop.address !=' => ''
			),
			'order' => array(
				'Flop.amount' => 'DESC'
			),
			'limit' => $limit,
			'recursive' => -1,
			'fields' => array('amount', 'address', 'created')
		));
	}
}
