<?php
App::uses('AppModel', 'Model');
/**
 * Roll Model
 *
 */
class Roll extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'ip_address' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
		'address' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
		'value' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'status' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
	);

	public function checkToday($ip, $address) {
		$rolls = $this->find('count', array(
			'conditions' => array(
				'DATE(Roll.created) = CURDATE()',
				'Roll.ip_address' => $ip
			)
		));

		if($rolls >= 3) {
			return true;
		}

		$roll = $this->find('first', array(
			'conditions' => array(
				'DATE(Roll.created) = CURDATE()',
				'Roll.ip_address' => $ip,
				'Roll.status' => 1
			)
		));

		if($roll) {
			return true;
		}

		return false;
	}

	public function getLastRollByIP($ip) {
		$roll = $this->find('first', array(
			'conditions' => array(
				'DATE(Roll.created) = CURDATE()',
				'Roll.ip_address' => $ip
			),
			'order' => array(
				'Roll.created' => 'DESC'
			)
		));

		return $roll;
	}
}
