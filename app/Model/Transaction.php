<?php
App::uses('AppModel', 'Model');
/**
 * Transaction Model
 *
 */
class Transaction extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'from_account' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
		'ip_address' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
		'transaction_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
	);

	public function saveTransaction($data=array()) {
		$this->create();
		if($this->save($data)) {
			return true;
		} else {
			return false;
		}
	}
}
